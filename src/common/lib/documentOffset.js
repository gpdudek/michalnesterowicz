import { window } from 'lib/window'

/**
 * Returns offset position of an element relative to the document body.
 *
 * Stolen from jQuery :trollface:
 *
 * @param  {DOMElement} element DOM element.
 * @return {Object}
 */
export default function documentOffset (element) {
  if (!element) {
    return { top: 0, left: 0 }
  }

  // IE <= 11
  if (!element.getClientRects().length) {
    return { top: 0, left: 0 }
  }

  const rect = element.getBoundingClientRect()

  if (rect.width || rect.height) {
    const { documentElement } = window.document
    return {
      top: rect.top + window.pageYOffset - documentElement.clientTop,
      left: rect.left + window.pageXOffset - documentElement.clientLeft
    }
  }

  return rect
}
