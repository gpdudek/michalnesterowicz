/**
 * News Archive page
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { needs } from 'react-component-needs'
import imagesLoaded from 'imagesloaded'

import NewsItem from './NewsItem'

import style from './newsArchive.css'

export class NewsArchive extends Component {

  componentDidMount () {
    this.doLayout()
  }

  componentDidUpdate () {
    this.doLayout()
  }

  doLayout () {
    const Masonry = require('masonry-layout')

    const doMasonry = () => {
      this.masonry = this.refs.wrap && new Masonry(this.refs.wrap, {
        itemSelector: `.${style.oneNewsArchive}`,
        percentPosition: true,
        gutter: 0,
        transitionDuration: 0,
        horizontalOrder: true
      })
    }

    setTimeout(() => {
      doMasonry()
      imagesLoaded('#NewsArchive', doMasonry)
    }, 0)
  }

  render () {
    return (
      <section className={style.newsArchive} id='NewsArchive'>
        <h2 className={style.title}>News Archive</h2>
        <div className={style.newsArchiveWrap} ref='wrap'>
          {this.props.news.map((news, index) => (
            <NewsItem
              news={news}
              key={index}
            />
          ))}
        </div>
      </section>
    )
  }
}

export default connect(
  (state) => ({
    news: state.content.news
  })
)(needs(
  (ownProps) => ({
    // no 'needs' because News component fires it
    condition: ownProps.news !== null
  })
)(NewsArchive))
