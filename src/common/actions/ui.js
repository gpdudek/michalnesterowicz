export const TOGGLE_NEWS_ARCHIVE = 'TOGGLE_NEWS_ARCHIVE'

export function toggleNewsArchive () {
  return {
    type: TOGGLE_NEWS_ARCHIVE
  }
}
