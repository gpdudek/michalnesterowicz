import { combineReducers } from 'redux'
import { promiseAwaitReducer } from 'redux-promise-await-middleware'

import contact from './contact'
import content from './content'
import ui from './ui'

const reducers = combineReducers({
  contact,
  content,
  ui,
  _promises: promiseAwaitReducer()
})

export default reducers
