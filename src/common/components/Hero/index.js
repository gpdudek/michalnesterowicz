/**
 * Hero page
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { needs } from 'react-component-needs'

import { getHeroPhotos } from 'actions/content'

import style from './hero.css'

export class Hero extends Component {

  constructor (props) {
    super(props)

    this.state = {
      numberPhoto: 1
    }
  }

  setPhoto = (number) => {
    this.setState ({
      numberPhoto: number
    })
  }

  componentDidMount () {
    setInterval(() => {
      if (this.state.numberPhoto > this.props.photos.length) {
        this.setState ({
          numberPhoto: 1
        })
      } else {
        this.setPhoto(this.state.numberPhoto + 1)
      }
    }, 10000) 
  }

  render(){
    return(
      <section className={style.heroWrap}>

        <div className={(this.state.numberPhoto === 1) ? style.heroActive : style.hero}>
          <img className={style.heroImg} src='/images/Jumbotron/jumbotron2b.jpg'/>
          <div className={style.heroText}>
            <h1 className={style.name}>Michał Nesterowicz</h1>
            <h1 className={style.role}>CONDUCTOR</h1>
          </div>
        </div>

        {this.props.photos.map((photo, index) => {
          const key = index + 2
          return (
            <div
              key={key}
              className={(this.state.numberPhoto === key) ? style.heroActive : style.hero}
            >
              <img className={style.heroImg} src={photo.fields.photo.fields.file.url} />
              <div className={(photo.fields.whiteText) ? style.darkReviewWrap : style.reviewWrap}>
                <h3 className={style.review}>{photo.fields.quote}</h3>
                <span className={style.reviewLine} />
                <h3 className={style.review}>{photo.fields.author}</h3>
              </div>
            </div>
          )
        })}

        <ul className={style.slickDots}>
          <li className={(this.state.numberPhoto === 1) ? style.slickActive : null}>
            <button type='button' onClick={() => this.setPhoto(1)}></button>
          </li>
          {this.props.photos.map((photo, index) => {
            const key = index + 2
            return (
              <li
                key={key}
                className={(this.state.numberPhoto === key) ? style.slickActive : null}
              >
                <button type='button' onClick={() => this.setPhoto(key)}></button>
              </li>
            )
          })}
        </ul>
      </section>
    )
  }
}

export default connect(
  (state) => ({
    photos: state.content.heroPhotos
  }),
  (dispatch, ownProps) => ({
    getHeroPhotos: () => dispatch(getHeroPhotos())
  })
)(needs(
  (ownProps) => ({
    condition: ownProps.photos !== null,
    needs: ownProps.getHeroPhotos
  })
)(Hero))
