import express from 'express'
import bodyParser from 'body-parser'

import healthcheck from './endpoints/healthcheck'
import contact from './endpoints/contact'

/**
 * Register API middleware.
 *
 * @return {Express}
 */
export default function () {
  const router = express.Router()

  router.use(bodyParser.json())

  router.get('/healthcheck', healthcheck)
  router.post('/contact', contact)

  return router
}
