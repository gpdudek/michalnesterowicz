export function dateBounds (date) {
  const dateFrom = date ? new Date(date.getTime()) : null
  if (dateFrom) {
    dateFrom.setHours(0)
    dateFrom.setMinutes(0)
  }
  const dateTo = date ? new Date(date.getTime()) : null
  if (dateTo) {
    dateTo.setHours(23)
    dateTo.setMinutes(59)
  }
  return {
    dateFrom,
    dateTo
  }
}

export function isDateInRange(date, dateFrom, dateTo) {
  return dateFrom && dateTo && date >= dateFrom && date <= dateTo
}
