import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { connect } from 'react-redux'
import { needs } from 'react-component-needs'
import ReactMarkdown from 'react-markdown'

import { getNewsItem } from 'actions/content'

import SocialMedia from 'components/SocialMedia'
import slugify from 'lib/slugify'

import style from 'components/News/news.css'

export class SingleNews extends Component {
  static propTypes = {
    newsItem: PropTypes.object.isRequired,
    sole: PropTypes.bool
  }

  static defaultProps = {
    sole: false
  }

  getUrl () {
    const { newsItem } = this.props
    return `/news/${newsItem.sys.id}/${slugify(newsItem.fields.title)}`
  }

  renderNewsItem () {
    const { newsItem } = this.props

    return (
      <div className={style.newsColumns}>
        <div className={style.newsColumn}>
          <h4 className={style.titleNewsColumn}>{newsItem.fields.title}</h4>
        </div>
        <div className={style.newsColumn}>
          <ReactMarkdown source={newsItem.fields.content} className={style.textNewsColumn} />
        </div>
        <div className={style.newsColumn}>
          <img className={style.imgNewsColumn} src={newsItem.fields.photo.fields.file.url} />
          <SocialMedia
            url={this.getUrl()}
            title={newsItem.fields.title}
          />
        </div>
      </div>
    )
  }

  render () {
    if (!this.props.sole) {
      return this.renderNewsItem()
    }

    const { newsItem } = this.props

    return (
      <section className={style.soleNews}>
        <Helmet>
          <meta name='description' content={newsItem.fields.content} />
          <meta property='og:title' content={newsItem.fields.title} />
          <meta property='og:type' content='article' />
          <meta property='og:url' content={this.getUrl()} />
          <meta property='og:description' content={newsItem.fields.content} />
          <meta property='og:site_name' content='Michał Nesterowicz' />
          <meta name='twitter:card' content='summary' />
          <meta name='twitter:title' content={newsItem.fields.title} />
          <meta name='twitter:description' content={newsItem.fields.content} />
          <meta property='og:image' content={newsItem.fields.photo.fields.file.url} />
          <meta name='twitter:image' content={newsItem.fields.photo.fields.file.url} />
          <link rel='canonical' href={this.getUrl()} />
        </Helmet>
        <div className={style.newsWraper}>
          {this.renderNewsItem()}
        </div>
      </section>
    )
  }
}

export default connect(
  (state) => ({
    newsItem: state.content.newsItem,
    sole: true
  }),
  (dispatch, ownProps) => ({
    getNewsItem: () => dispatch(getNewsItem(ownProps.match.params.id))
  })
)(needs(
  (ownProps) => ({
    condition: ownProps.newsItem && ownProps.newsItem.sys.id === ownProps.match.params.id,
    needs: ownProps.getNewsItem
  })
)(SingleNews))
