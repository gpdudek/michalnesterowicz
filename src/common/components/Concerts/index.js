/**
 * Concerts page
 */

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { needs } from 'react-component-needs'
import DayPicker from 'react-day-picker'

import {
  getConcerts,
  filterConcerts
} from 'actions/content'
import {
  fullDate,
  amPmFromDate
} from 'lib/dateFormatting'
import { isDateInRange } from 'lib/dateTools'
import scrollAnimateTo from 'lib/scrollTo'

import style from './concerts.css'
import calendarStyle from './calendar.css'
import BottomLines from 'components/BottomLines'
import Pagination from 'components/Pagination'

export class Concerts extends Component {

  static propTypes = {
    loadConcerts: PropTypes.func.isRequired,
    displayConcerts: PropTypes.func.isRequired
  }

  setPage = (page) => {
    this.props.displayConcerts(page, this.props.isArchive)
  }

  toggleArchive = () => {
    this.props.displayConcerts(1, !this.props.isArchive)
  }

  showAll = () => {
    this.props.displayConcerts(1, this.props.isArchive, true)
  }

  search = () => {
    const {
      isArchive,
      displayConcerts
    } = this.props

    const query = this.refs.search.value
    if (query && query.trim().length > 0) {
      displayConcerts(1, isArchive, true, query.trim())
    } else {
      displayConcerts(1, isArchive)
    }
  }

  handleDayClick = (day) => {
    this.props.displayConcerts(1, false, false, null, day)
    setTimeout(() => {
      scrollAnimateTo(`.${style.concertsTableRowActive.split(' ')[0]}`, 100, 300)
    }, 100)
  }

  getSeason = () => {
    const today = new Date()
    const year = (today.getMonth() >= 7) ? today.getFullYear() : today.getFullYear() + 1
    return `${year - 2000}/${year + 1 - 2000}`
  }

  renderConcert (concert, index) {
    const {
      dateFrom,
      dateTo
    } = this.props

    const date = new Date(concert.fields.date)
    const isHighlighted = (dateFrom && dateTo)
      ? isDateInRange(new Date(concert.fields.date), dateFrom, dateTo)
      : index === 0

    return (
      <tr key={index} className={isHighlighted ? style.concertsTableRowActive : style.concertsTableRow}>
        <td className={style.concertsTableDesc}>{fullDate(concert.fields.date)}<br/>{concert.fields.time}</td>
        <td className={style.concertsTableDesc}>{concert.fields.place}</td>
        <td className={style.concertsDescPerformers}>{concert.fields.performersList}</td>
        <td className={style.concertsDescProgram}>{concert.fields.programText}</td>
        <td className={style.concertsTableDesc}>
          {!!concert.fields.ticketsUrl && <a
            href={concert.fields.ticketsUrl}
            target='_blank'
            className={style.concertsButton}
          >TICKETS</a>}
        </td>
      </tr>
    )
  }

  render () {
    const {
      concerts,
      search
    } = this.props

    return (
      <section className={style.concerts} id='Concerts'>

        <div className={style.concertsHeader}>
          <div className={style.concertsHeaderLeft}>
            <h2 className={style.concertsTitle}>Concerts</h2>
            <h3 className={style.concertsSubtitle}>
              {this.props.isArchive ? 'Past Performances' : 'Upcoming Performances'}
            </h3>
            <div className={style.searchWrap}>
              <input
                type='text'
                className={style.searchInput}
                onChange={this.search}
                value={search || ''}
                placeholder='Search'
                ref='search'
              />
            </div>
          </div>
          <div className={style.concertsHeaderRight}>
            <div className={style.concertsRightCalendar}>
              <DayPicker
                onDayClick={this.handleDayClick}
                selectedDays={this.props.all.map((concert) => new Date(concert.fields.date))}
                classNames={calendarStyle}
                firstDayOfWeek={1}
              />
            </div>
            <h1 className={style.concertsSeason}>SEASON {this.getSeason()}</h1>
          </div>
        </div>

        <div className={style.concertsWraper}>
          <table className={style.concertsTable}>
            <thead>
              <tr>
                <th className={style.concertsHeading}>DATE</th>
                <th className={style.concertsHeading}>PLACE</th>
                <th className={style.concertsHeadingPerformers}>PERFORMERS</th>
                <th className={style.concertsHeadingProgram}>PROGRAM</th>
                <th className={style.concertsHeading}>
                  <button className={style.concertsButtonArchive} onClick={this.toggleArchive}>
                    {this.props.isArchive ? 'UPCOMING' : 'ARCHIVE'}
                  </button>
                </th>
              </tr>
            </thead>
            <tbody>
              {concerts && concerts.length > 0 && concerts.map((concert, index) => this.renderConcert(concert, index))}
              {(!concerts || !concerts.length) && (
                <tr>
                  <td colSpan={5} className={style.noConcerts}>No concerts found</td>
                </tr>
              )}
            </tbody>
          </table>

          <div className={style.concertsBottomWrap}>
            <Pagination
              total={this.props.totalPages}
              current={this.props.currentPage}
              onClick={this.setPage}
              scrollTo='#Concerts'
              buttonText='Show All'
              onButtonClick={this.showAll}
            />
            <BottomLines className={style.bottomLinesOut} />
          </div>
        </div>
      </section>
    )
  }
}

export default connect(
  (state) => ({
    all: state.content.concerts.all,
    concerts: state.content.concerts.visible,
    totalPages: state.content.concerts.totalPages,
    currentPage: state.content.concerts.currentPage,
    isArchive: state.content.concerts.isArchive,
    search: state.content.concerts.search,
    dateFrom: state.content.concerts.dateFrom,
    dateTo: state.content.concerts.dateTo
  }),
  (dispatch, ownProps) => ({
    loadConcerts: () => dispatch(getConcerts()),
    displayConcerts: (page = 1, isArchive = false, all = false, search = null, date = null) => {
      return dispatch(filterConcerts(page, isArchive, all, search, date))
    }
  })
)(needs(
  (ownProps) => ({
    condition: ownProps.all !== null,
    needs: ownProps.loadConcerts
  })
)(Concerts))
