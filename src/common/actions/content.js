import cms from 'lib/cms'

export const GET_HERO_PHOTOS = 'GET_HERO_PHOTOS'
export const GET_BIO = 'GET_BIO'
export const GET_CONCERTS = 'GET_CONCERTS'
export const GET_NEWS = 'GET_NEWS'
export const GET_NEWS_ITEM = 'GET_NEWS_ITEM'
export const GET_PHOTOS = 'GET_PHOTOS'
export const GET_MEDIA = 'GET_MEDIA'
export const GET_CONTACTS = 'GET_CONTACTS'
export const FILTER_CONCERTS = 'FILTER_CONCERTS'
export const RESET_CONCERTS = 'RESET_CONCERTS'
export const RESET_NEWS = 'RESET_NEWS'
export const SHOW_NEWS_ITEM = 'SHOW_NEWS_ITEM'

export function getHeroPhotos () {
  return {
    type: GET_HERO_PHOTOS,
    promise: cms.getHeroPhotos()
  }
}

export function getBio () {
  return {
    type: GET_BIO,
    promise: cms.getBio()
  }
}

export function getConcerts () {
  return {
    type: GET_CONCERTS,
    promise: cms.getConcerts()
  }
}

export function filterConcerts (page = 1, isArchive = false, all = false, search = null, date = null) {
  return {
    type: FILTER_CONCERTS,
    data: {
      page,
      isArchive,
      all,
      search,
      date
    }
  }
}

export function getNews () {
  return {
    type: GET_NEWS,
    promise: cms.getNews()
  }
}

export function getNewsItem (id) {
  return {
    type: GET_NEWS_ITEM,
    promise: cms.getNewsItem(id)
  }
}

export function getPhotos () {
  return {
    type: GET_PHOTOS,
    promise: cms.getPhotos()
  }
}

export function getMedia () {
  return {
    type: GET_MEDIA,
    promise: cms.getMedia()
  }
}

export function getContacts () {
  return {
    type: GET_CONTACTS,
    promise: cms.getContacts()
  }
}

export function resetConcerts () {
  return {
    type: RESET_CONCERTS
  }
}

export function resetNews () {
  return showNewsItem(0)
}

export function showNewsItem (number) {
  return {
    type: SHOW_NEWS_ITEM,
    data: {
      number
    }
  }
}
