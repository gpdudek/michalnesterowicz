/** 
 * Photos page
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { needs } from 'react-component-needs'

import { getPhotos } from 'actions/content'

import style from './photo.css'

export class Photos extends Component {

  constructor () {
    super()

    this.state = {
      fullPhoto: null
    }
  }

  setFullPhoto = (photo) => {
    if (photo === null) {
      this.setState({ fullPhoto: null })
      return
    }

    if (photo < 0) {
      this.setState({ fullPhoto: this.props.photos.length - 1 })
      return
    }

    this.setState({
      fullPhoto: photo >= this.props.photos.length ? 0 : photo
    })
  }

  renderPhoto = (photo, index) => {
    return (
      <td
        key={index}
        className={style.PhotosTableDescription}
      >
        <div className={style.certainPhoto}>
          <img
            className={style.photosImg}
            src={photo.fields.small.fields.file.url}
            onClick={() => this.setFullPhoto(index)}
          />
          <div className={style.photoDowload}>
            <h5 className={style.photoDownloadHeadline}>Download</h5>
            <p className={style.photoDowloadLinks}>
              <a
                href={photo.fields.small.fields.file.url}
                className={style.photoDowloadLink}
                target='_blank'
                download
              >Small</a>
              <span className={style.photoDowloadLine}>|</span>
              <a
                href={photo.fields.medium.fields.file.url}
                className={style.photoDowloadLink}
                target='_blank'
                download
              >Medium</a>
              <span className={style.photoDowloadLine}>|</span>
              <a
                href={photo.fields.full.fields.file.url}
                className={style.photoDowloadLink}
                target='_blank'
                download
              >Full</a>
            </p>
          </div>
        </div>
      </td>
    )
  }

  render () {
    const { photos } = this.props
    const { fullPhoto } = this.state
    const bigPhoto = fullPhoto !== null ? photos[fullPhoto] : null

    return (
      <section className={style.photos} id='Photos'>
        <h2 className={style.photosTitle}>Photos</h2>
        <div className={(bigPhoto) ? style.cartainBigPhotoActive : style.cartainBigPhoto}>
          <div className={style.crossIcon} onClick={() => this.setFullPhoto(null)}>
            <span className={style.crossOneLine} />
            <span className={style.crossTwoLine} />
          </div>
          <i className={'fa fa-angle-right ' + style.icoNext} onClick={() => this.setFullPhoto(fullPhoto + 1)} />
          {!!bigPhoto && <img className={style.photosImgBig} src={bigPhoto.fields.medium.fields.file.url} />}
          <i className={'fa fa-angle-left ' + style.icoNextLeft} onClick={() => this.setFullPhoto(fullPhoto - 1)} />
        </div>
        <div className={style.photosItemsWrap}>
          <table className={style.photosTableWrap}>
            <tbody>
              <tr className={style.photosItems}>
                {photos.map(this.renderPhoto)}
              </tr>
            </tbody>
          </table>
        </div>
      </section>
    )
  }

}

export default connect(
  (state) => ({
    photos: state.content.photos
  }),
  (dispatch) => ({
    getPhotos: () => dispatch(getPhotos())
  })
)(needs(
  (ownProps) => ({
    condition: ownProps.photos !== null,
    needs: ownProps.getPhotos
  })
)(Photos))
