export const fullMonths = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
]

export const shortMonths = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec'
]

export const numberMonths = [
  '01',
  '02',
  '03',
  '04',
  '05',
  '06',
  '07',
  '08',
  '09',
  '10',
  '11',
  '12'
]

export const weekDays = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday'
]

export const shortWeekDays = [
  'Sun',
  'Mon',
  'Tue',
  'Wed',
  'Thu',
  'Fri',
  'Sat'
]

export function fullDate (rawDate) {
  const date = new Date(rawDate)
  return `${date.getDate()} ${dateAsMonthYear(rawDate)}`
}

export function timeFromDate (rawDate) {
  const date = new Date(rawDate)
  return `${leadingZero(date.getHours())}:${leadingZero(date.getMinutes())}`
}

export function amPmFromDate (rawDate) {
  const date = new Date(rawDate)
  let hours = date.getHours()
  let amPm = 'AM'
  if (hours > 12) {
    hours = hours - 12
    amPm = 'PM'
  }
  return `${hours}:${leadingZero(date.getMinutes())} ${amPm}`
}

export function dateAsMonthYear (rawDate, separator = ' ') {
  const date = new Date(rawDate)

  const month = fullMonths[date.getMonth()]
  const year = date.getFullYear()

  return `${month}${separator}${year}`
}

export function leadingZero (number) {
  return number < 10 ? '0' + number + '' : number + ''
}
