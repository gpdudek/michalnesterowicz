/*
 * Bottom lines.
 */
import React, { Component } from 'react'
import PropTypes from 'prop-types'

import style from './bottomLines.css'

export default class BottomLines extends Component {

  static propTypes = {
    className: PropTypes.string
  }

  static defaultProps = {
    className: ''
  }

  render() {
    const { className } = this.props

    return (
        <div className={className + ' ' + style.sectionBottomLines}>
          <span className={style.lineleft} />
          <span className={style.lineRight} />
        </div>
      )
  }
}
