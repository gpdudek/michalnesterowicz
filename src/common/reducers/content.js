import {
  GET_HERO_PHOTOS,
  GET_BIO,
  GET_CONCERTS,
  GET_NEWS,
  GET_NEWS_ITEM,
  GET_PHOTOS,
  GET_MEDIA,
  GET_CONTACTS,
  FILTER_CONCERTS,
  RESET_CONCERTS,
  SHOW_NEWS_ITEM
} from 'actions/content'
import {
  dateBounds,
  isDateInRange
} from 'lib/dateTools'

const initialState = {
  heroPhotos: null,
  bio: null,
  concerts: {
    all: null,
    upcoming: [],
    past: [],
    visible: [],
    currentPage: 1,
    totalPages: 0,
    isArchive: false,
    search: '',
    dateFrom: null,
    dateTo: null
  },
  news: null,
  newsItem: null,
  newsItemNumber: 0,
  photos: null,
  media: null,
  contacts: null
}

const DEFAULT_PER_PAGE = 10

function paginate (items, page = 1, perPage = DEFAULT_PER_PAGE) {
  return {
    visible: items.slice(page * perPage - perPage, page * perPage),
    totalPages: Math.ceil(items.length / perPage),
    currentPage: page
  }
}

function searchObject (term, object, fields = []) {
  if (!term) {
    return true
  }

  return fields.some((field) => object.hasOwnProperty(field) &&
    typeof object[field] === 'string' &&
    object[field].toLowerCase().includes(term.toLowerCase())
  )
}

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_HERO_PHOTOS:
      return {
        ...state,
        heroPhotos: action.result
      }

    case GET_BIO:
      return {
        ...state,
        bio: action.result
      }

    case GET_CONCERTS:
      // set midnight as break point, so concerts from today are included in future, not in archive
      const breakDate = new Date()
      breakDate.setHours(0)
      breakDate.setMinutes(0)
      breakDate.setSeconds(0, 0)

      const upcoming = []
      const past = []

      action.result.items.map((concert) => {
        const date = new Date(concert.fields.date)
        if (date >= breakDate) {
          upcoming.push(concert)
        } else {
          past.push(concert)
        }
      })

      return {
        ...state,
        concerts: {
          ...state.concerts,
          all: action.result.items,
          upcoming,
          past: past.reverse(),
          ...paginate(
            state.concerts.isArchive ? past : upcoming,
            state.concerts.currentPage,
            10
          )
        }
      }

    case FILTER_CONCERTS:
      const {
        all,
        search,
        date
      } = action.data
      let {
        isArchive,
        page
      } = action.data

      const {
        dateFrom,
        dateTo
      } = dateBounds(date)

      // if date range is in the past then we need to use archive
      const today = new Date()
      if (dateTo && dateTo < today) {
        isArchive = true
      }

      const concerts = isArchive ? state.concerts.past : state.concerts.upcoming

      // detect on which page is the highlighted concert and switch to that page
      let highlightedPosition = false
      concerts.map((concert, index) => {
        if (isDateInRange(new Date(concert.fields.date), dateFrom, dateTo)) {
          highlightedPosition = index
        }
      })
      if (highlightedPosition !== false) {
        page = all ? 1 : Math.floor(highlightedPosition / DEFAULT_PER_PAGE) + 1
      }

      return {
        ...state,
        concerts: {
          ...state.concerts,
          ...paginate(
            concerts.filter((concert) => searchObject(search, concert.fields, ['performersList', 'place', 'programText'])),
            page,
            all ? 999 : DEFAULT_PER_PAGE
          ),
          search,
          isArchive,
          dateFrom,
          dateTo
        }
      }

    case RESET_CONCERTS:
      return {
        ...state,
        concerts: {
          ...state.concerts,
          ...paginate(
            state.concerts.upcoming,
            1,
            DEFAULT_PER_PAGE
          ),
          search: '',
          isArchive: false,
          dateFrom: null,
          dateTo: null
        }
      }

    case GET_NEWS:
      return {
        ...state,
        news: action.result.items,
        newsItem: action.result.items ? action.result.items[0] : null,
        newsItemNumber: 0
      }

    case GET_NEWS_ITEM:
      return {
        ...state,
        newsItem: action.result
      }

    case GET_PHOTOS:
      return {
        ...state,
        photos: action.result.items
      }

    case GET_MEDIA:
      return {
        ...state,
        media: action.result.items
      }

    case GET_CONTACTS:
      return {
        ...state,
        contacts: action.result.items
      }

    case SHOW_NEWS_ITEM:
      return {
        ...state,
        newsItem: state.news ? state.news[action.data.number] : null,
        newsItemNumber: action.data.number
      }
  }

  return state
}
