import axios from 'axios'

export const SEND_MESSAGE = 'SEND_MESSAGE'

export function sendMessage (message) {
  return {
    type: SEND_MESSAGE,
    data: {
      message
    },
    promise: axios.post('/api/contact', message)
  }
}
