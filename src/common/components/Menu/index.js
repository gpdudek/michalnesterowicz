/**
 * Menu page
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'

import {
  resetConcerts,
  resetNews
} from 'actions/content'

import style from './menu.css'

export class Menu extends Component {
 
  constructor (){
    super()

    this.state = {
      active: false
    }
  }

  toggleMenu = () => {
    if (this.state.active === true) {
      this.setState({
        active: false
      })
    } else {
      this.setState({
        active: true
      })
    }
  }

  render () {
    const {
      resetNews,
      resetConcerts
    } = this.props

    return (
      <header className={style.header}>
        <div className={style.headerContent}>
          <a
            href='https://en-gb.facebook.com/people/Michal-Nesterowicz/100000739314466'
            target='_blank'
            className={style.fbLink}
          >
            <i className='fa fa-facebook-f' />
          </a>
          <ul className={style.menuItems}>
            <li className={style.menuItem}>
              <a href='#Bio'
                className={style.menuLink}
              >Bio</a>
            </li>
            <li className={style.menuItem}>
              <a href='#Concerts'
                className={style.menuLink}
                onClick={resetConcerts}
              >Concerts</a>
            </li>
            <li className={style.menuItem}>
              <a href='#News'
                className={style.menuLink}
                onClick={resetNews}
              >News</a>
            </li>
            <li className={style.menuItem}>
              <a href='#Photos'
                className={style.menuLink}
              >Photos</a>
            </li>
            <li className={style.menuItem}>
              <a href='#Media'
                className={style.menuLink}
              >Media</a>
            </li>
            <li className={style.menuItem}>
              <a href='#Contact'
                className={style.menuLink}
              >Contact</a>
            </li>
          </ul>

          <div className={style.menuNavigation}>
            <div className={(this.state.active == true) ? style.iconActive : style.icon} onClick={this.toggleMenu}>
              <span className={style.lineOne} />
              <span className={style.lineTwo} />
            </div>

            <div className={(this.state.active == true) ? style.menuDropDownActive : style.menuDropDown}>
              <div className={style.discoverMobile}>

                <ul className={style.menuItemsDropDown}>
                  <li className={style.menuItemDropDown}>
                    <a href='#Bio'
                      className={style.menuLink}
                    >Bio</a>
                  </li>
                  <li className={style.menuItemDropDown}>
                    <a href='#Concerts'
                      className={style.menuLink}
                      onClick={resetConcerts}
                    >Concerts</a>
                  </li>
                  <li className={style.menuItemDropDown}>
                    <a href='#News'
                      className={style.menuLink}
                      onClick={resetNews}
                    >News</a>
                  </li>
                  <li className={style.menuItemDropDown}>
                    <a href='#Photos'
                      className={style.menuLink}
                    >Photos</a>
                  </li>
                  <li className={style.menuItemDropDown}>
                    <a href='#Media'
                      className={style.menuLink}
                    >Media</a>
                  </li>
                  <li className={style.menuItemDropDown}>
                    <a href='#Contact'
                      className={style.menuLink}
                    >Contact</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>

        </div>
      </header>
    )
  }
}

export default connect(
  null,
  (dispatch) => ({
    resetConcerts: () => dispatch(resetConcerts()),
    resetNews: () => dispatch(resetNews())
  })
)(Menu)
