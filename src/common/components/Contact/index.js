/**
 * Contact page
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { needs } from 'react-component-needs'

import { sendMessage } from 'actions/contact'
import { getContacts } from 'actions/content'

import style from './contact.css'

export class Contact extends Component {
  constructor () {
    super()

    this.state = {
      name: true,
      email: true,
      content: true
    }
  }

  send = () => {
    const message = {
      name: this.getValue('name'),
      email: this.getValue('email'),
      content: this.getValue('content')
    }

    if (!message.name || !message.email || !message.content) {
      return false;
    }

    this.props.sendMessage(message)
  }

  getValue = (field) => {
    const input = this.refs[field]
    if (!input) {
      return false
    }

    const value = input.value.trim()
    if (value.length === 0) {
      this.setState({
        [field]: value
      })
      return false
    }

    this.setState({
      [field]: true
    })
    return value
  }

  render () {
    const {
      status,
      contacts
    } = this.props

    return (
      <section className={style.contact} id='Contact'>
        <div className={style.contactLeftSide}>
          <h2 className={style.contactTitle}>Contact</h2>
          {status === 'sent' && <div className={style.sent}>Thank you for your message,<br />I will try to respond shortly.</div>}
          {status !== 'sent' && (
            <div className={style.inputWraper}>
              <div className={!this.state.name ? style.errorInputWrap : style.inputWrap}>
                <input className={style.inputContacttext} placeholder='' ref='name' />
                <label className={style.inputLabel}>Name/Surname:</label>
              </div>

              <div className={!this.state.email ? style.errorInputWrap : style.inputWrap}>
                <input className={style.inputContacttext} placeholder='' ref='email' />
                <label className={style.inputLabel}>Email Adress:</label>
              </div>

              <div className={!this.state.content ? style.errorInputWrap : style.inputWrap}>
                <textarea className={style.inputTextarea} placeholder='' ref='content' />
                <label className={style.inputLabel}>Message:</label>
                {status === 'sending' && <div className={style.sending}>Sending...</div>}
                <button className={style.contactBlackButton} onClick={this.send}>Send</button>
              </div>
            </div>
          )}
          <span className={style.contactLineleft} />
          <p className={style.copyright}>Copyright &copy; Michał Nesterowicz {(new Date()).getFullYear()}</p>
        </div>

        <div className={style.contactRightSide}>
          <h2 className={style.contactTitleBlack}>World Wide Management</h2>

          <div className={style.contactHPWrap}>
            <a href='http://www.harrisonparrott.com/' target='_blank'>
              <img className={style.hplogo} src='/images/HPlogo.png' />
            </a>
            {contacts.map((contact, index) => (
              <p key={index} className={style.contactHP}>
                {contact.fields.name}{' '}{contact.fields.phone}
                <a
                  href={`mailto:${contact.fields.email}`}
                  className={contact.fields.highlight ? style.yellowContactHP : null}
                >{contact.fields.email}</a>                
              </p>
            ))}
          </div>

          <span className={style.contactLineRight} />
        </div>
      </section>
    )
  }
}

export default connect(
  (state) => ({
    status: state.contact.status,
    contacts: state.content.contacts
  }),
  (dispatch) => ({
    sendMessage: (message) => dispatch(sendMessage(message)),
    getContacts: () => dispatch(getContacts())
  })
)(needs(
  (ownProps) => ({
    condition: ownProps.contacts !== null,
    needs: ownProps.getContacts
  })
)(Contact))
