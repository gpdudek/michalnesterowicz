/**
 * Pagination component
 */

import React, { Component } from 'react'
import PropTypes from 'prop-types'

import scrollAnimateTo from 'lib/scrollTo'

import style from './pagination.css'

export default class Pagination extends Component {

  static propTypes = {
    total: PropTypes.number.isRequired,
    current: PropTypes.number.isRequired,
    onClick: PropTypes.func,
    scrollTo: PropTypes.string,
    scrollMargin: PropTypes.number,
    buttonText: PropTypes.string,
    onButtonClick: PropTypes.func,
    buttonScrollTo: PropTypes.string,
    buttonScrollMargin: PropTypes.number,
    showAll: PropTypes.bool
  }

  static defaultProps = {
    onClick: () => null,
    scrollTo: null,
    scrollMargin: 60,
    buttonText: null,
    onButtonClick: () => null,
    buttonScrollTo: null,
    buttonScrollMargin: 60,
    showAll: false
  }

  componentWillMount () {
    this.compressedLeft = false
    this.compressedRight = false
  }

  handleClick = (page) => {
    const {
      scrollTo,
      scrollMargin,
      onClick
    } = this.props
    
    onClick(page)

    if (scrollTo) {
      scrollAnimateTo(scrollTo, scrollMargin, 300)
    }
  }

  handleButtonClick = () => {
    const {
      buttonScrollTo,
      buttonScrollMargin,
      onButtonClick
    } = this.props

    onButtonClick()

    if (buttonScrollTo) {
      scrollAnimateTo(buttonScrollTo, buttonScrollMargin, 300)
    }
  }

  getPages () {
    const {
      total,
      current,
      showAll
    } = this.props

    if (showAll) {
      return this.getAllPages()
    }

    // How many surrounding pages do we allow, before
    // we "compress" them
    const offset = 3

    let min = 1
    let max = total

    let pages = []

    // Check if the current page plus the offset
    // is bigger than the total amount of pages
    if (current + offset > total) {
      max = total
      this.compressedRight = false
    } else {
      max = current + offset
      this.compressedRight = true
    }

    // Check if the current page minus the offset is 0
    if (current - offset < 1) {
      min = 1
      this.compressedLeft = false
    } else {
      min = (current - offset)
      this.compressedLeft = true
    }

    const firstPage = this.getPageElement(1)
    const lastPage = this.getPageElement(total, this.compressedRight ? style.compressedRight : '')

    pages.push(firstPage)

    for (let index = min + 1; index < max; index++) {
      pages.push(this.getPageElement(index))
    }

    // If there's only 1 page, firstPage is the same as the lastPage
    if (min !== max) {
      pages.push(lastPage)
    }

    return pages
  }

  getAllPages () {
    const {
      total,
      current
    } = this.props

    const pages = []
    for (let index = 1; index <= total; index++) {
      pages.push(this.getPageElement(index))
    }

    return pages
  }

  getPageElement (page, customClass = '') {
    const {
      onClick,
      current
    } = this.props

    const className = (current === page) ? style.active : style.page

    return (
      <li
        key={page}
        className={customClass + ' ' + className}
        onClick={(current === page) ? () => {} : () => this.handleClick(page)}
      >{page}</li>
    )
  }

  getPaginationClass () {
    if (this.compressedLeft && this.compressedRight) return style.paginationBoth
    if (this.compressedLeft) return style.paginationLeft
    if (this.compressedRight) return style.paginationRight

    return style.pagination
  }

  render () {
    const {
      total,
      buttonText
    } = this.props

    if (!total || total < 2) return false

    return (
      <div className={style.wrap}>
        <ul className={this.getPaginationClass()}>
          {this.getPages()}
          {!!buttonText && (
            <li className={style.buttonWrap}>
              <button className={style.button} onClick={this.handleButtonClick}>{buttonText}</button>
            </li>
          )}
        </ul>
      </div>
    )
  }
}
