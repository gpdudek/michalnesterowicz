/**
 * Wraps around the config to be reusable on both server and in the browser.
 *
 * If in browser then it's read from a global `__CONFIG__` var.
 *
 * If on server then it's required from `/config.json` and then checked for
 * ENV variables that can overwrite it.
 */
let config = {}

if (process.env.BROWSER === true) {
  config = window.__CONFIG__
} else {
  config = require('../../../config.json')
  // ENV variables could overwrite it
  for (var key in config) {
    config[key] = process.env[key] || config[key]
  }
}

export default config
