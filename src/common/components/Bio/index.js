/**
 * Bio page
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { needs } from 'react-component-needs'
import ReactMarkdown from 'react-markdown'

import { getBio } from 'actions/content'

import style from './bio.css'
import BottomLines from 'components/BottomLines'

export class Bio extends Component {

  render () {
    const { bio } = this.props
    if (bio === null) {
      return false
    }

    return (
      <section className={style.bio} id='Bio'>
        <h2 className={style.bioTitle}>Bio</h2>
        <div className={style.bioWraper}>
          <div className={style.bioLeftSide}>
            <div className={style.bioImgWraper}>
              <img className={style.bioImg} src={bio.fields.photo.fields.file.url} />
              <small className={style.bioImgTitle}>{bio.fields.photoCaption}</small>
            </div>
            <ReactMarkdown source={bio.fields.leftColumn} />
          </div>
          <ReactMarkdown source={bio.fields.rightColumn} className={style.bioRightSide} />
        </div>
        <BottomLines />
      </section>
    )
  }
}

export default connect(
  (state) => ({
    bio: state.content.bio
  }),
  (dispatch, ownProps) => ({
    getBio: () => dispatch(getBio())
  })
)(needs(
  (ownProps) => ({
    condition: ownProps.bio !== null,
    needs: ownProps.getBio
  })
)(Bio))
