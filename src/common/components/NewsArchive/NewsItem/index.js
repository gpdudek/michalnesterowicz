/**
 * News Item component to be used on News Archive
 */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactMarkdown from 'react-markdown'

import style from '../newsArchive.css'
import { fullDate } from 'lib/dateFormatting'

export default class NewsItem extends Component {

  static propTypes = {
    news: PropTypes.object.isRequired
  }

  constructor () {
    super()
    this.state = {
      expanded: false
    }
  }

  render () {
    const { news } = this.props

    return (
      <div className={style.oneNewsArchive}>
        <img className={style.img} src={news.fields.photo.fields.file.url} />
        <div className={style.titleWrap}>
          <h6 className={style.titleCertainNews}>{news.fields.title}</h6>
          <p className={style.data}>{fullDate(news.fields.date)}</p>
        </div>
        <ReactMarkdown
          source={news.fields.content}
          className={style.textWrap}
        />
       
      </div>
    )
  }
}
