import mailgun from 'mailgun-js'

export default function (req, res) {
  const mailgunClient = mailgun({
    apiKey: process.env.MAILGUN_API_KEY,
    domain: process.env.MAILGUN_DOMAIN
  })

  const {
    name,
    email,
    content
  } = req.body

  mailgunClient.messages().send({
    from: `${name} <${email}>`,
    to: process.env.CONTACT_EMAIL,
    subject: `Message from ${name}`,
    text: content
  }, (err, result) => {
    if (err) {
      console.log(err)
    }
    res.status(201).end()
  })
}
