import React from 'react'
import ReactDOM from 'react-dom'

import { window, document } from 'lib/window'
import documentOffset from 'lib/documentOffset'
import raf from 'lib/raf'

let scrollToElement = null
let scrollToMargin = 0
let scrollTimeout = null

/**
 * Scrolls the browser window to the given element.
 *
 * The scrolling itself is debounced to 50ms to make sure the page doesn't jump multiple
 * times if for some reason this is called multiple times.
 *
 * @param {String|HTMLElement|Component} item         CSS selector or HTMLElement or React Component to scroll to.
 * @param {Number}                       scrollMargin Margin from top of the window.
 * @param {Number}                       duration     Duration of the scroll, in ms. Default: 0.
 */
export default function scrollTo (item, scrollMargin = 200, duration = 0) {
  // store in statics
  scrollToElement = item
  scrollToMargin = scrollMargin

  // debounce the action to 50ms
  clearTimeout(scrollTimeout)
  scrollTimeout = setTimeout(() => {
    let target = null

    // support CSS selectors
    if (typeof scrollToElement === 'string') {
      try {
        target = document.querySelector(scrollToElement)
      } catch (e) {
        target = null
      }
    }

    // support React Components
    if (scrollToElement instanceof React.Component) {
      target = ReactDOM.findDOMNode(scrollToElement)
    }

    // make sure we have smth
    if (!target || !(target instanceof window.HTMLElement)) {
      return
    }

    // calculate the target scroll position and scroll
    const scrollToPosition = Math.max(documentOffset(target).top - scrollToMargin, 0)
    animateScrollTo(scrollToPosition, duration)

    // reset
    scrollToElement = null
    scrollToMargin = 0
  }, 50)
}

/**
 * Animates scrolling the page to the specified position.
 *
 * @param {Number} targetPosition Y position to scroll to.
 * @param {Number} duration       Duration of the scroll in ms.
 */
export function animateScrollTo (targetPosition, duration) {
  // if 0 or empty duration then just jump
  if (!duration) {
    window.scrollTo(0, targetPosition)
    return
  }

  // convert to seconds
  duration = duration / 1000

  const currentPosition = window.scrollY
  let currentTime = 0

  // easing 'easeOutSine' equation function from https://github.com/danro/easing-js/blob/master/easing.js
  const ease = (pos) => Math.sin(pos * (Math.PI / 2))

  let animationId = null

  // single animation frame tick
  const tick = () => {
    // animate at 60 fps
    currentTime += 1 / 60

    const progress = currentTime / duration
    const newPosition = currentPosition + ((targetPosition - currentPosition) * ease(progress))

    window.scrollTo(0, newPosition)

    if (progress >= 1) {
      raf.unsubscribe(animationId)
    }
  }

  // start ticking
  animationId = raf.subscribe(tick)
}
