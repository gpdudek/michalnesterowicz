import { TOGGLE_NEWS_ARCHIVE } from 'actions/ui'

const initialState = {
  showNewsArchive: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_NEWS_ARCHIVE:
      return {
        ...state,
        showNewsArchive: !state.showNewsArchive
      }

    default:
      return state
  }
}
