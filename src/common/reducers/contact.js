import { SEND_MESSAGE } from 'actions/contact'

const initialState = {
  status: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SEND_MESSAGE + '//PENDING':
      return {
        ...state,
        status: 'sending'
      }

    case SEND_MESSAGE:
      return {
        ...state,
        status: 'sent'
      }

    default:
      return state
  }
}
