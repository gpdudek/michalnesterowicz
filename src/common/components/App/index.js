import React, { Component } from 'react'
import Helmet from 'react-helmet'
import { Route } from 'react-router-dom'
import { connect } from 'react-redux'

import Menu from 'components/Menu'
import Bio from 'components/Bio'
import Concerts from 'components/Concerts'
import Contact from 'components/Contact'
import Hero from 'components/Hero'
import Media from 'components/Media'
import News from 'components/News'
import Photos from 'components/Photos'
import NewsArchive from 'components/NewsArchive'
import SingleNews from 'components/SingleNews'

export class App extends Component {
  render () {
    const { showNewsArchive } = this.props

    return (
      <div>
        <Helmet
          defaultTitle='Michał Nesterowicz - Conductor - Official Site'
          titleTemplate='%s | Michał Nesterowicz - Conductor - Official Site'
        >
          <html lang='en' />
          <meta charset='utf-8' />
          <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
          <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
          <meta name='viewport' content='width=device-width, initial-scale=1.0' />
          <script src='https://use.fontawesome.com/524f472f7b.js'></script>
        </Helmet>
        <Menu />
        <Route path='/news/:id/:slug' component={SingleNews} />
        <Hero />
        <Bio />
        <Concerts />
        <News />
        {showNewsArchive && <NewsArchive />}
        <Photos />
        <Media />
        <Contact />
      </div>
    )
  }
}

export default connect(
  (state) => ({
    showNewsArchive: state.ui.showNewsArchive
  })
)(App)
