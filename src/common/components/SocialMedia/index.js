/*
 * Social Media icons.
 */
import React, { Component } from 'react'
import PropTypes from 'prop-types'

import config from 'config'

import style from './social.css'

export default class SocialMedia extends Component {
  static propTypes = {
    url: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
  }

  render () {
    const {
      url,
      title
    } = this.props
    const fullUrl = `${config.BASE_URL}${url}`

    const facebookUrl = `https://www.facebook.com/dialog/share?app_id=${config.FACEBOOK_APP_ID}&display=popup&href=${encodeURIComponent(fullUrl)}`
    const twitterUrl = `http://twitter.com/share?text=${encodeURIComponent(title)}&url=${encodeURIComponent(fullUrl)}`
    const linkedInUrl = `https://www.linkedin.com/shareArticle?title=${encodeURIComponent(title)}&url=${encodeURIComponent(fullUrl)}&mini=true`

    return (
      <div className={style.social}>
        <p className={style.share}>Share this</p>
        <ul className={style.shareLinks}>
          <li className={style.shareLink}>
            <a
              href={facebookUrl}
              target='_blank'
              className={'fa fa-facebook ' + style.socialIco}
            />
          </li>
          <li className={style.shareLink}>
            <a
              href={twitterUrl}
              target='_blank'
              className={'fa fa-twitter ' + style.socialIco}
            />
          </li>
          <li className={style.shareLink}>
            <a
              href={linkedInUrl}
              target='_blank'
              className={'fa fa-linkedin ' + style.socialIco}
            />
          </li>
        </ul>
      </div>
    )
  }
}
