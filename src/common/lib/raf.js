/*
* rAF
*
* Global request animation frame implementation that objects can subscribe to
*/

import { window } from './window'

// shim layer with setTimeout fallback
window.requestAnimationFrame = (function () {
  return window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  function (callback) {
    window.setTimeout(callback, 1000 / 60)
  }
})()

var _subscribers = {}
var _isRunning = false
var _lastID = 1
var _prefix = 'RAF_'

/**
 * Iterate through subscribed callback and calls them with current time
 */
var _updateSubscribers = (time) => {
  for (let id in _subscribers) {
    _subscribers[id](time)
  }
}

/**
 * Executes on every requestAnimationFrame tick
 * Simply updates subscribed functions
 */
var _tick = (time) => {
  _updateSubscribers(time)
  if (_isRunning) window.requestAnimationFrame(_tick)
}

export default {
  /**
   * Removes a callback based on its token.
   */
  subscribe (callback) {
    if ((typeof callback) !== 'function') return

    var id = _prefix + _lastID++
    _subscribers[id] = callback

    // We have a new subscriber, start the clock!
    this.start()
    return id
  },

  /**
   * Removes a callback based on its token.
   */
  unsubscribe (id) {
    if (_subscribers[id]) {
      delete _subscribers[id]
    }
  },

  // Starts the requestAnimationFrame timer
  start () {
    if (_subscribers.length < 1 || _isRunning) {
      return // no elements or already running -> don't do anything
    }
    _isRunning = true
    _tick()
  },

  stop () {
    _isRunning = false
  }
}
