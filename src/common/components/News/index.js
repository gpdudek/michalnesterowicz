/**
 * News page
 */

import React, {Component} from 'react'
import { connect } from 'react-redux'
import { needs } from 'react-component-needs'

import {
  getNews,
  showNewsItem
} from 'actions/content'
import { toggleNewsArchive } from 'actions/ui'

import style from './news.css'

import BottomLines from 'components/BottomLines'
import { SingleNews } from 'components/SingleNews'
import Pagination from 'components/Pagination'

export class News extends Component {

  render () {
    const {
      news,
      newsItem,
      newsItemNumber,
      toggleNewsArchive,
      showNewsArchive,
      showNewsItem
    } = this.props

    if (news.length === 0) {
      return false
    }

    if (!newsItem) {
      return false
    }

    return (
      <section className={style.news} id='News'>
        <h2 className={style.newsTitle}>News</h2>

        <div className={style.newsWraper}>

          <SingleNews newsItem={newsItem} />

          <BottomLines className={style.bottomLinesWrapOut} />

          <Pagination
            total={Math.min(news.length, 5)}
            current={newsItemNumber + 1}
            onClick={(number) => showNewsItem(Math.max(number - 1, 0))}
            scrollTo='#News'
            buttonText={showNewsArchive ? 'Hide Archive' : 'Show Archive'}
            onButtonClick={toggleNewsArchive}
            buttonScrollTo='#NewsArchive'
            showAll
          />
        </div>
      </section>
    )
  }
}

export default connect(
  (state) => ({
    news: state.content.news,
    newsItem: state.content.newsItem,
    newsItemNumber: state.content.newsItemNumber,
    showNewsArchive: state.ui.showNewsArchive
  }),
  (dispatch, ownProps) => ({
    getNews: () => dispatch(getNews()),
    toggleNewsArchive: () => dispatch(toggleNewsArchive()),
    showNewsItem: (number) => dispatch(showNewsItem(number))
  })
)(needs(
  (ownProps) => ({
    condition: ownProps.news !== null,
    needs: ownProps.getNews
  })
)(News))
