/** 
 * Media page
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import ReactPlayer from 'react-player'
import { needs } from 'react-component-needs'

import { getMedia } from 'actions/content'
import { window } from 'lib/window'

import style from './media.css'

export class Media extends Component {

  constructor () {
    super()

    this.state = {
      fullMedia: null
    }
  }

  setFullMedia = (media) => {
    if (media === null) {
      this.setState({ fullMedia: null })
      return
    }

    if (media < 0) {
      this.setState({ fullMedia: this.props.media.length - 1 })
      return
    }

    this.setState({
      fullMedia: media >= this.props.media.length ? 0 : media
    })
  }

  renderMedia = (media, index) => {
    return (
      <td key={index} className={style.mediaItem} onClick={() => this.setFullMedia(index)}>
        <div className={style.mediaItemInside}>
          <ReactPlayer url={media.fields.url} width={300} height={200} className={style.mediaItemPlayer} />
          <h6 className={style.mediaItemTitle}>{media.fields.title}</h6>
          <p className={style.mediaItemDescription}>{media.fields.description}</p>
        </div>
      </td>
    )
  }

  render() {
    const { media } = this.props
    const { fullMedia } = this.state
    const bigMedia = fullMedia !== null ? media[fullMedia] : null

    return (
      <section className={style.media} id='Media'>
        <h2 className={style.mediaTitle}>Media</h2>
        <div className={(bigMedia) ? style.bigMediaActive : style.bigMedia}>
          <div className={style.crossIcon} onClick={() => this.setFullMedia(null)}>
            <span className={style.crossOneLine} />
            <span className={style.crossTwoLine} />
          </div>
          <i className={'fa fa-angle-right ' + style.icoNext} onClick={() => this.setFullMedia(fullMedia + 1)} />
          {!!bigMedia && (
            <ReactPlayer
              className={style.bigMediaPlayer}
              url={bigMedia.fields.url}
              width={window.innerWidth * 0.9}
              height={window.innerHeight * 0.95}
              playing
              controls
            />
          )}
          <i className={'fa fa-angle-left ' + style.icoNextLeft} onClick={() => this.setFullMedia(fullMedia - 1)} />
        </div>
        <div className={style.mediaItemsWrap}>
          <table className={style.mediaItemsTable}>
            <tbody>
              <tr className={style.mediaItems}>
                {media.map(this.renderMedia)}
              </tr>
            </tbody>
          </table>
        </div>
      </section>
    )
  }
}

export default connect(
  (state) => ({
    media: state.content.media
  }),
  (dispatch) => ({
    getMedia: () => dispatch(getMedia())
  })
)(needs(
  (ownProps) => ({
    condition: ownProps.media !== null,
    needs: ownProps.getMedia
  })
)(Media))
